# Open-source SIP client for Android & IOS

Based on React Native and [ReactNativePJSIP](https://bitbucket.org/smiledeveloper/voip-app-based-pjsip-react-native/)

![Scheme](docs/screenshot-app.jpg)

## Setup

1. **Clone the repo**

  ```
  $ git clone https://bitbucket.org/smiledeveloper/voip-app-based-pjsip-react-native.git
  $ cd voip-app-based-pjsip-react-native
  ```

2. **Install dependencies**:

  ```
  $ npm install
  ```

3. **Running on Android**:

  ```
  $ react-native run-android
  ```

4. **Running on iOS:**

  ```
  $ react-native run-ios
  ```

